﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using IX.Identity.Business.Account.Command.Register;
using IX.Identity.Business.Tests.Common;
using IX.Identity.DataAccess;
using IX.SDK.PasswordHashing;
using IX.SDK.Result;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace IX.Identity.Business.Test.Account.Command
{
    [Collection("QueryCollection")]
    public class RegisterCommandTest
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly IMediator _mediator;
        private readonly IPasswordHashingService _passwordHashingService;

        public RegisterCommandTest(TestFixture fixture)
        {
            _applicationDbContext = fixture.Context;
            _mediator = fixture.Mediator;
            _passwordHashingService = fixture.HashingService;
        }

        [Fact]
        public async Task RegisterUser()
        {
            var command = new RegisterCommand("test", "test", new[] {1});
            var handler = new RegisterCommandHandler(_applicationDbContext, _passwordHashingService);
            var result = await handler.Handle(command, CancellationToken.None);
            
            
            Assert.NotNull(result);
            Assert.IsAssignableFrom<Result>(result);
            Assert.True(result.Success);

            var user = await _applicationDbContext.Users.FirstOrDefaultAsync(CancellationToken.None);
            
            Assert.NotNull(user);
            
            result = await handler.Handle(command, CancellationToken.None);
            
            Assert.NotNull(result);
            Assert.IsAssignableFrom<Result>(result);
            Assert.False(result.Success);
            
            
            var commandWithNotExistsRole = new RegisterCommand("test2", "test", new[] {1,2});
            handler = new RegisterCommandHandler(_applicationDbContext, _passwordHashingService);
            result = await handler.Handle(command, CancellationToken.None);
            
            Assert.NotNull(result);
            Assert.IsAssignableFrom<Result>(result);
            Assert.False(result.Success);
        }
    }
}