﻿using System.Security.Cryptography;
using IX.Identity.DataAccess;
using MediatR;
using Xunit;
using IX.Identity.DataAccess.Tests.Base;
using IX.Mediator.Tests.Base;
using IX.SDK.PasswordHashing;

namespace IX.Identity.Business.Tests.Common
{
    public class TestFixture
    {

        public ApplicationDbContext Context { get; }
        
        public IMediator Mediator { get; }

        public IPasswordHashingService HashingService { get; }
        
        public TestFixture()
        {
            Context = DbContextBase.GetDbContext();
            Mediator = MediatorBase.CreateMediatorMock();
            HashingService = new PasswordHashingService(
                new PasswordHashingServiceOptions(), 
                new SaltGenerateService(new RNGCryptoServiceProvider()));
        }
    }
    
    [CollectionDefinition("QueryCollection")]
    public class QueryCollection : ICollectionFixture<TestFixture> { }
}