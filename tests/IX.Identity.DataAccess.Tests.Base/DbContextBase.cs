﻿using System;
using System.Data.Common;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace IX.Identity.DataAccess.Tests.Base
{
    public class DbContextBase
    {
        private static DbConnection CreateInMemoryDatabase()
        {
            var connection = new SqliteConnection(@"DataSource=:memory:");

            connection.Open();

            return connection;
        }
        private static DbContextOptionsBuilder<ApplicationDbContext> GetDbOptionsBuilder()
        {
            var builder = new DbContextOptionsBuilder<ApplicationDbContext>().UseSqlite(CreateInMemoryDatabase());

            return builder;
        }

        public static ApplicationDbContext GetDbContext()
        {
            var optionsBuilder = GetDbOptionsBuilder();
            ApplicationDbContext dbContext = new ApplicationDbContext(optionsBuilder.Options);
            dbContext.Database.EnsureDeleted();
            dbContext.Database.EnsureCreated();
            return dbContext;
        }
        
        public static void Destroy(ApplicationDbContext context)
        {
            context.Database.EnsureDeleted();

            context.Dispose();
        }
    }
}