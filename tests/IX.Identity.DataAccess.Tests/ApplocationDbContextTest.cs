using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using IX.Identity.DataAccess.Tests.Base;
using IX.Identity.Domain.AggregateModels;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace IX.Identity.DataAccess.Tests
{
    public class ApplocationDbContextTest : DbContextBase
    {
        [Fact]
        public void AddRolesTest()
        {
            var fixture = new Fixture();
            var roles = new List<Role>
            {
                fixture.Build<Role>().Create()
            };

            var dbContext = GetDbContext();

            var result = dbContext.Roles.ToList();
            
            Assert.Single(result);
            
            dbContext.Add(new Role("sdfsda"));
            dbContext.SaveChanges();
            var resultGet = dbContext.Roles.FirstOrDefault(x => x.Id == 2);
            
            Assert.NotNull(resultGet);
            
            dbContext.AddRange(roles);
            dbContext.SaveChanges();
            result = dbContext.Roles.ToList();
            
            Assert.Equal(3, result.Count);
            Assert.Equal(3, result.Last().Id);
        }
        
        [Fact]
        public void AddAccountTest()
        {
            var fixture = new Fixture();
            var dbContext = GetDbContext();

            var account = fixture.Build<User>().Create();
            var role = dbContext.Roles.FirstOrDefault();
            account.AddRoles(role);
            dbContext.Users.Add(account);
            dbContext.SaveChanges();
            var result = dbContext.Users.Include(x => x.Roles).FirstOrDefault();
            Assert.NotNull(result);
            Assert.Single(result.Roles);
            
            
            var newRole = fixture.Build<Role>().Create();
            dbContext.AddRange(newRole);
            dbContext.SaveChanges();
            result.AddRoles(newRole);
            dbContext.Update(result);
            dbContext.SaveChanges();
            result = dbContext.Users.Include(x => x.Roles).FirstOrDefault();
            Assert.NotNull(result);
            Assert.Equal(2, result.Roles.Count);
            
            
            result.RemoveRoles(newRole);
            dbContext.Update(result);
            dbContext.SaveChanges();
            result = dbContext.Users.Include(x => x.Roles).FirstOrDefault();
            Assert.NotNull(result);
            Assert.Single(result.Roles);
        }
    }
}