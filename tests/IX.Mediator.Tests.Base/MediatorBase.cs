using System;
using System.Linq;
using System.Threading;
using MediatR;
using Moq;
using Xunit;

namespace IX.Mediator.Tests.Base
{
    public class MediatorBase
    {
        public static IMediator CreateMediatorMock()
        {
            var repository = new MockRepository(MockBehavior.Default);
            var mediator = repository.Create<IMediator>();
            mediator.Setup(x => x.Publish(It.IsAny<object>(), It.IsAny<CancellationToken>()))
                .Verifiable(string.Empty);
            return repository.Of<IMediator>().First();
        }
    }
}