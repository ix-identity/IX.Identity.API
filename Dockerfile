FROM mcr.microsoft.com/dotnet/sdk:5.0

EXPOSE 80

WORKDIR /app
COPY . .

ENTRYPOINT ["dotnet", "IX.Identity.API.dll"]