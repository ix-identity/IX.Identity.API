﻿using System.Threading;
using System.Threading.Tasks;
using IX.Identity.DataAccess;
using IX.Identity.DataAccess.Extensions;
using IX.SDK.PasswordHashing;
using IX.SDK.Result;
using MediatR;

namespace IX.Identity.Business.Account.Command.Register
{
    public class RegisterCommandHandler : IRequestHandler<RegisterCommand, Result>
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly IPasswordHashingService _hashingService;
        public RegisterCommandHandler(ApplicationDbContext dbContext, IPasswordHashingService hashingService)
        {
            _dbContext = dbContext;
            _hashingService = hashingService;
        }
        
        public async Task<Result> Handle(RegisterCommand request, CancellationToken cancellationToken)
        {
            var userExists = await _dbContext.Users.UserExist(request.Login);
            var rolesExists = await _dbContext.Roles.RolesExist(request.Roles);
            
            if (userExists)
                return Result.Fail("User exists");
            if (!rolesExists)
                return Result.Fail("Roles doesn't exists");
            
            var hash = _hashingService.Hash(request.Password, request.Login);
            var roles = await _dbContext.Roles.GetRoles(request.Roles);
            await _dbContext.Users.RegisterUser(request.Login, hash, roles);
            
            await _dbContext.SaveChangesAsync(cancellationToken);
            
            return Result.Ok(true);
        }
    }
}