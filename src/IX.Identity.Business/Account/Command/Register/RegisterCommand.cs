﻿using IX.SDK.Result;
using MediatR;

namespace IX.Identity.Business.Account.Command.Register
{
    public class RegisterCommand : IRequest<Result>
    {
        public RegisterCommand(string login, string password, int[] roles)
        {
            Login = login;
            Password = password;
            Roles = roles;
        }
        public string Login { get; }
        public string Password { get; }
        public int[] Roles { get; }
    }
}