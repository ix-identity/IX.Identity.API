﻿using FluentValidation;
using IX.Identity.DataAccess;
using IX.SDK.PasswordHashing;
using Mapster;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace IX.Identity.Business
{
    public static class Bootstrap
    {
        public static IServiceCollection AddBusiness(
            this IServiceCollection services,
            IConfiguration configuration)
        {
            TypeAdapterConfig.GlobalSettings.Scan(typeof(Bootstrap).Assembly);
            return services
                .AddDataAccess(configuration)
                .AddValidatorsFromAssembly(typeof(Bootstrap).Assembly)
                .AddMediatR(typeof(Bootstrap).Assembly)
                .AddPasswordHashingService();
        }
    }
}