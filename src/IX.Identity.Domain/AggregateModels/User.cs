﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IX.Identity.Domain.AggregateModels
{
    public class User
    {
        
        public long Id { get; private set; }
        
        public string Email { get; private set; }
        public string Password { get; private set; }

        private readonly HashSet<Role> roles;
        
        public User(string email, string password)
        {
            roles = new HashSet<Role>();
            Email = email;
            Password = password;
        }
        
        public IReadOnlyCollection<Role> Roles => roles;

        public void AddRoles(IEnumerable<Role> addRoles)
        {
            if (!addRoles.Any()) return;
            foreach (var role in addRoles)
            {
                roles.Add(role);
            }
        }
        
        public void RemoveRoles(IEnumerable<Role> removeRoles)
        {
            if (!removeRoles.Any()) return;
            
            foreach (var role in removeRoles)
            {
                roles.Remove(role);
            }
        }
        
        
        public void AddRoles(Role addRole)
        {
            if (addRole is not { }) return;
            
            roles.Add(addRole);
        }
        
        public void RemoveRoles(Role removeRole)
        {
            if (removeRole is not { }) return;
            
            roles.Remove(removeRole);
        }
    }
}