﻿using System.Collections.Generic;

namespace IX.Identity.Domain.AggregateModels
{
    public class Role
    {
        private static readonly string AdminTitle = "Админ";
        
        public static Role Admin { get; } = new Role(1, AdminTitle);  
        private Role(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public Role(string name)
        {
            Name = name;
        }
        public int Id { get; private set; }
        public string Name { get; private set; }
        public IReadOnlyCollection<User> Users { get; }
    }
}