﻿using System.Collections.Generic;
using System.Threading.Tasks;
using IX.Identity.Domain.AggregateModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace IX.Identity.DataAccess.Extensions
{
    public static class UserExtension
    {
        public static async Task<bool> UserExist(this DbSet<User> dbSet, string login)
        {
            return await dbSet.AnyAsync(user => user.Email == login);
        }
        
        public static async Task<EntityEntry<User>> RegisterUser(
            this DbSet<User> dbSet, 
            string login, 
            string hash, 
            IEnumerable<Role> roles)
        {
            var user = new User(login, hash);
            user.AddRoles(roles);
            return await dbSet.AddAsync(user);
        }
    }
}