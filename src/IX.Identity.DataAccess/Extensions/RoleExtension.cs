﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IX.Identity.Domain.AggregateModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace IX.Identity.DataAccess.Extensions
{
    public static class RoleExtension
    {
        public static async Task<bool> RolesExist(this DbSet<Role> dbSet, int[] roleIds)
        {
            var count = roleIds.Distinct().Count();
            var countInDbContext = await dbSet.Where(role => roleIds.Contains(role.Id)).CountAsync();
            return count == countInDbContext;
        }
        
        public static async Task<bool> RoleExist(this DbSet<Role> dbSet, int roleId)
        {
            return await dbSet.AnyAsync(role => role.Id == roleId);
        }
        
        
        public static async Task<IEnumerable<Role>> GetRoles(this DbSet<Role> dbSet, int[] roleIds)
        {
            var roles = await dbSet.Where(role => roleIds.Contains(role.Id)).ToListAsync();
            return roles;
        }
    }
}