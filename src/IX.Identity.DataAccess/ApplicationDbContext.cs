﻿using IX.Identity.Domain.AggregateModels;
using Microsoft.EntityFrameworkCore;

namespace IX.Identity.DataAccess
{
    public class ApplicationDbContext : DbContext
    {
        public virtual  DbSet<User> Users { get; set; }
        public virtual  DbSet<Role> Roles { get; set; }

        public ApplicationDbContext()
        {}
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");
            modelBuilder.ApplyConfigurationsFromAssembly(assembly: typeof(ApplicationDbContext).Assembly);
        }
    }

}