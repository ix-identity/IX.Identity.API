﻿using IX.Identity.Domain.AggregateModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IX.Identity.DataAccess.Configurations
{
    public class RoleConfiguration : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder
                .Property(x => x.Id)
                .ValueGeneratedOnAdd();
            
            builder.HasKey(x => x.Id)
                .HasName("PK_Role");

            builder.HasData(Role.Admin);
        }
    }
}