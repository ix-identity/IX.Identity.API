﻿using IX.Identity.Domain.AggregateModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IX.Identity.DataAccess.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder
                .Property(x => x.Id)
                .ValueGeneratedOnAdd();
            
            builder
                .HasKey(x => x.Id)
                .HasName("PK_User");
            
            builder.HasMany(x => x.Roles)
                .WithMany(x => x.Users);

        }
    }
}