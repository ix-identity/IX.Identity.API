﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IX.Identity.DataAccess.Migrations
{
    public partial class AddDefaultRole : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                schema: "public",
                table: "Roles",
                columns: new[] { "Id", "Name" },
                values: new object[] { 1, "Админ" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "public",
                table: "Roles",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
